#pragma once

#include <QTreeView>

class PlotPropertiesTreeView : public QTreeView
{
    Q_OBJECT

public:
    PlotPropertiesTreeView(QWidget *parent);
    ~PlotPropertiesTreeView();




protected:
    virtual void mouseReleaseEvent(QMouseEvent *ev) override;
    bool edit(const QModelIndex &index, EditTrigger trigger, QEvent *event) override;
};
