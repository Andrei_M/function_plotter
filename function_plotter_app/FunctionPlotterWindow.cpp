#include "FunctionPlotterWindow.h"

#include <memory>

#include <QComboBox>
#include "Interpreter.h"
#include "PlotPropertiesModel.h"
#include "CanvasProperties.h"

#include "TreeItem.h"
#include "ItemDelegateFactory.h"
#include "AbstractFunction.h"

FunctionPlotterWindow::FunctionPlotterWindow(QWidget *parent)
    : QMainWindow(parent)
    , mCanvasScene{std::make_shared<fun::CanvasScene>()}
{
    ui.setupUi(this);
    ui.mFunctionTypeComboBox->addItem("Function", FT_Function);
    ui.mFunctionTypeComboBox->addItem("Polar", FT_Polar);
    ui.mFunctionTypeComboBox->addItem("Parametric", FT_Parametric);

    connect(mCanvasScene.get(),
        &fun::CanvasScene::selectedChanged,
        this,
        &FunctionPlotterWindow::updateUiState);

    connect(ui.mFunctionTypeComboBox,
        static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
        this,
        &FunctionPlotterWindow::functionTypeChanged);
    connect(ui.mAddFunctionButton,
        &QAbstractButton::clicked,
        this,
        &FunctionPlotterWindow::addFunction);
    functionTypeChanged(0);

    connect(ui.mDrawFunctionButton,
        &QAbstractButton::clicked,
        this,
        &FunctionPlotterWindow::updateFunction);

    createActions();
    createMenus();

    initCanvasProperties();
    ui.mPlotCanvas->setCanvasProperties(mCanvasProps);
    ui.mPlotCanvas->setScene(mCanvasScene);
    initTableView();
}

void FunctionPlotterWindow::functionTypeChanged(int newIndex)
{
    QVariant itemData = ui.mFunctionTypeComboBox->currentData();
    FunctionType functionType = static_cast<FunctionType>(itemData.toInt());
    ui.mFunctionParamsGroup->setVisible(false);
    ui.mPolarParamsGroup->setVisible(false);
    ui.mParametricParamsGroup->setVisible(false);

    switch (functionType)
    {
    case FT_Function:
        ui.mFunctionParamsGroup->setVisible(true);
        break;

    case FT_Polar:
        ui.mPolarParamsGroup->setVisible(true);
        break;

    case FT_Parametric:
        ui.mParametricParamsGroup->setVisible(true);
        break;

    default:
        // not supported
        break;
    }
}

void FunctionPlotterWindow::addFunction()
{
    QVariant itemData = ui.mFunctionTypeComboBox->currentData();
    FunctionType functionType = static_cast<FunctionType>(itemData.toInt());
    QString functionExpression = QString::null;
    QString polarExpression = QString::null;
    QString parametricXExpression = QString::null;
    QString parametricYExpression = QString::null;

    switch (functionType)
    {
    case FT_Function:
        functionExpression = ui.mFunctionExpression->text();
        createFunction(functionExpression);
        break;

    case FT_Polar:
        polarExpression = ui.mPolarExpression->text();
        createPolarFunction(polarExpression);
        break;

    case FT_Parametric:
        parametricXExpression = ui.mParametricXExpression->text();
        parametricYExpression = ui.mParametricYExpression->text();
        createParametricFunction(parametricXExpression, parametricYExpression);
        break;

    default:
        // not supported
        break;
    }
}

void FunctionPlotterWindow::createFunction(QString const & yExpression)
{
}

void FunctionPlotterWindow::createPolarFunction(QString const & yExpression)
{

}

void FunctionPlotterWindow::createParametricFunction(QString const & xExpression, QString const & yExpression)
{

}

void FunctionPlotterWindow::updateFunction()
{
    auto expression = ui.mFunctionExpression->text().toStdString();
    auto functionItem = generateFunctionItem(expression);
    mCanvasScene->addFunction(functionItem);
}

std::shared_ptr<fun::AbstractFunction> FunctionPlotterWindow::generateFunctionItem(std::string const & expression)
{
    // setup the interpreter
    auto lexer = fun::Lexer(expression);
    auto parser = fun::Parser(lexer);
    auto ast = parser.parse();
    auto interpreter = std::make_shared<fun::Interpreter>(ast);
    interpreter->setExpression(expression);

    auto functionItem = std::make_shared<fun::AbstractFunction>();
    functionItem->setInterpreter(interpreter);
    return functionItem;
}

void FunctionPlotterWindow::initTableView()
{
    // setup the items
    auto invisibleRootItem = new TreeItem(nullptr);
    auto canvasRoot = makeCanvasPropertiesTreeItem();
    canvasRoot->setParent(invisibleRootItem);

    // setup the model
    auto treeModel = new PlotPropertiesModel(ui.mPropertiesTree);
    treeModel->setRootItem(invisibleRootItem);
    connect(treeModel, &QAbstractItemModel::dataChanged, this, &FunctionPlotterWindow::canvasPropertiesChanged);


    // connect to the view
    ui.mPropertiesTree->setModel(treeModel);
    ui.mPropertiesTree->setIndentation(10);
    ui.mPropertiesTree->setItemDelegateForColumn(1, new Delegates::CheckBoxDelegate(this));
    //ui.mPropertiesTree->setEditTriggers(QAbstractItemView::DoubleClicked
    //                                  | QAbstractItemView::SelectedClicked);
}

void FunctionPlotterWindow::createActions()
{
    mRemoveFunctionAction = new QAction("Remove", this);
    mRemoveFunctionAction->setEnabled(false);
    connect(mRemoveFunctionAction,
        &QAction::triggered,
        ui.mPlotCanvas,
        &PlotCanvas::removeSelectedFunction);
    mRemoveFunctionAction->setShortcut(Qt::Key_Delete);
}

void FunctionPlotterWindow::createMenus()
{
    ui.mFunctionMenu->addAction(mRemoveFunctionAction);
}

void FunctionPlotterWindow::updateUiState()
{
    mRemoveFunctionAction->setEnabled(mCanvasScene->hasSelectedFunction());
}



void FunctionPlotterWindow::initCanvasProperties()
{
    mCanvasProps = std::make_shared<CanvasProperties>();
}

TreeItem * FunctionPlotterWindow::makeCanvasPropertiesTreeItem()
{
    // the canvas
    TreeItem * root = new TreeItem(mCanvasProps->getProperties());
    root->setTitle("Canvas");
    root->setTag("canvas color");
    root->setIcon(QPixmap(":/FunctionPlotterWindow/icons/graph_properties.png"));

    new TreeItem(root, "origin visible", "Origin Point");

    // the grid
    auto gridItem = new TreeItem(root, "grid visible", "Grid");
    new TreeItem(gridItem, "grid color", "Color");
    new TreeItem(gridItem, "grid spacing", "Spacing");

    // the axes
    auto axesItem = new TreeItem(root, "axes visible", "Axes");
    new TreeItem(axesItem, "axes width", "Width");
    new TreeItem(axesItem, "axes color", "Color");
    new TreeItem(axesItem, "large ticks visible", "Large ticks");
    new TreeItem(axesItem, "small ticks visible", "Small ticks");
    new TreeItem(axesItem, "numbers visible", "Numbers");
    new TreeItem(axesItem, "numbers color", "Numbers color");

    return root;
}

void FunctionPlotterWindow::canvasPropertiesChanged()
{
    ui.mPlotCanvas->update();
}
