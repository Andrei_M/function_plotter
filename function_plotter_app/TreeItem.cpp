#include "TreeItem.h"

#include "PropertiesMap.h"
#include <algorithm>
#include <assert.h>

TreeItem::TreeItem(std::shared_ptr<PropertiesMap> propertiesMap)
    : mParent{ }
    , mPropertiesMap{ propertiesMap }
{

}

TreeItem::TreeItem(TreeItem* parent, std::string const & tag, std::string const & title, QIcon const & icon)
    : mParent{ }
    , mTag{ tag }
    , mTitle{ title }
{
    setParent(parent);
}

TreeItem::~TreeItem()
{
    for (auto child : mChildren) {
        delete child;
    }
}

QVariant TreeItem::data(int column, int role) const
{
    switch (role)
    {
    case Qt::EditRole:
    case Qt::DisplayRole:
    {
        if (column == 0) {
            return QString::fromStdString(mTitle);
        }
        else if (column == 1) {
            return getValue();
        }

        // return default (empty)
        return QVariant();
    }
    case Qt::CheckStateRole:
    {
        if (column == 1) {
            auto value = getValue();
            if (value.type() == QVariant::Bool) {
                bool boolValue = value.toBool();
                return boolValue ? Qt::Checked : Qt::Unchecked;
            } else {
                return QVariant();
            }
        }
        break;
    }
    case Qt::DecorationRole:
    {
        if (column == 0) {
            return icon();
        }
    }
    default:
        return QVariant();
    }

    return QVariant();
}

bool TreeItem::setData(int column, const QVariant &value)
{
    if (column != 1)
        return false;

    setValue(value);
    return true;
}

void TreeItem::setTag(std::string const & tag)
{
    mTag = tag;
}

void TreeItem::setTitle(std::string const & title)
{
    mTitle = title;
}


QVariant TreeItem::getValue() const
{
    return getValueOfTag(mTag);
}

QVariant TreeItem::getValueOfTag(std::string const & tag) const
{
    if (mPropertiesMap) {
        return (*mPropertiesMap)[tag];
    } else if (mParent) {
        return mParent->getValueOfTag(tag);
    } 

    // default (invalid) value
    return QVariant();
}

void TreeItem::setValue(QVariant const & value)
{
    setValueOfTag(mTag, value);
}

void TreeItem::setValueOfTag(std::string const & tag, QVariant const & value)
{
    if (mPropertiesMap) {
        (*mPropertiesMap)[tag] = value;
    } else if (mParent) {
        mParent->setValueOfTag(tag, value);
    }
}

void TreeItem::setIcon(QIcon const & icon)
{
    mIcon = icon;
}

void TreeItem::addChild(TreeItem* child)
{
    if (!hasChild(child))
    {
        mChildren.push_back(child);
        child->setParent(this);
    }
}

void TreeItem::removeChild(TreeItem* child)
{
    mChildren.remove(child);
    child->setParent(nullptr);
}

void TreeItem::setParent(TreeItem * parent)
{
    if (mParent)
    {
        if (mParent != parent)
        {
            mParent->removeChild(this);
            mParent = parent;
            mParent->addChild(this);
        }
    } else {
        mParent = parent;
        mParent->addChild(this);
    }
}

bool TreeItem::hasChild(TreeItem * child)
{
    auto it = std::find(mChildren.cbegin(), mChildren.cend(), child);
    return it != mChildren.cend();
}

int TreeItem::row() const
{
    if (mParent) {
        auto it = std::find(mParent->mChildren.cbegin(), mParent->mChildren.cend(), this);
        if (it != mParent->mChildren.cend()) {
            int index = std::distance(mParent->mChildren.cbegin(), it);
            return index;
        }
        assert("Should have found the position already");
    }

    return 0;
}

