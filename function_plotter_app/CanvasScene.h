#pragma once

#include <QObject>

#include <memory>
#include <vector>

namespace fun {

class AbstractFunction;

class CanvasScene : public QObject
{
    Q_OBJECT

public:
    CanvasScene();
    ~CanvasScene();

    void addFunction(std::shared_ptr<AbstractFunction> function);
    void removeFunction(std::shared_ptr<AbstractFunction> function);
    std::vector<std::shared_ptr<AbstractFunction>> functions() const { return mFunctions; }

    float minX() const { return mMinX; }
    float maxX() const { return mMaxX; }
    float minY() const { return mMinY; }
    float maxY() const { return mMaxY; }

    std::shared_ptr<AbstractFunction> getFunctionAt(float const & x, float const & y);
    void selectFunction(std::shared_ptr<AbstractFunction> function);
    std::shared_ptr<AbstractFunction> getSelectedFunction() const { return mSelectedFunction; }
    bool hasSelectedFunction() const { return getSelectedFunction() != nullptr; }
    

signals:
    void sceneChanged();
    void selectedChanged();

private:
    std::vector<std::shared_ptr<AbstractFunction>> mFunctions;
    float mMinX{-100};
    float mMaxX{+100};
    float mMinY{-100};
    float mMaxY{+100};

private:
    std::shared_ptr<AbstractFunction> mSelectedFunction;
};

}
