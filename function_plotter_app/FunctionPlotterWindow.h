#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_FunctionPlotterWindow.h"

#include <memory>

#include "CanvasScene.h"

class CanvasProperties;
class TreeItem;

namespace fun {
    class AbstractFunction;
}

class FunctionPlotterWindow : public QMainWindow
{
    Q_OBJECT

public:
    FunctionPlotterWindow(QWidget *parent = Q_NULLPTR);

private slots:
    void functionTypeChanged(int newIndex);
    void addFunction();
    void canvasPropertiesChanged();
    void updateUiState();

private:
    void createFunction(QString const & yExpression);
    void createPolarFunction(QString const & yExpression);
    void createParametricFunction(QString const & xExpression, QString const & yExpression);

    void updateFunction();
    std::shared_ptr<fun::AbstractFunction> generateFunctionItem(std::string const & expression);

    void initTableView();
    void createActions();
    void createMenus();

    void initCanvasProperties();

    TreeItem * makeCanvasPropertiesTreeItem();

private:
    enum FunctionType {
        FT_Function,
        FT_Polar,
        FT_Parametric,
    };

    Ui::FunctionPlotterWindowClass ui;
    std::shared_ptr<CanvasProperties> mCanvasProps;
    std::shared_ptr<fun::CanvasScene> mCanvasScene;

    // ui actions
    QAction * mRemoveFunctionAction;
};
