TEMPLATE = app
TARGET = function_plotter_app
QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
INCLUDEPATH += $$PWD/../function_parser_lib
DEPENDPATH += $$PWD/../function_parser_lib

CONFIG(debug, debug|release) {
    LIBS += -L$$OUT_PWD/../function_parser_lib/debug -lfunction_parser
}
CONFIG(release, debug|release) {
    LIBS += -L$$OUT_PWD/../function_parser_lib/release -lfunction_parser
}

RESOURCES += FunctionPlotterWindow.qrc
FORMS = FunctionPlotterWindow.ui

HEADERS = AbstractFunction.h \
    CanvasProperties.h \
    CanvasScene.h \
    FunctionPlotterWindow.h \
    ItemDelegateFactory.h \
    PlotCanvas.h \
    PlotPropertiesModel.h \
    PlotPropertiesTreeView.h \
    PropertiesMap.h \
    TreeItem.h

SOURCES = main.cpp \
    AbstractFunction.cpp \
    CanvasProperties.cpp \
    CanvasScene.cpp \
    FunctionPlotterWindow.cpp \
    ItemDelegateFactory.cpp \
    PlotCanvas.cpp \
    PlotPropertiesModel.cpp \
    PlotPropertiesTreeView.cpp \
    TreeItem.cpp
