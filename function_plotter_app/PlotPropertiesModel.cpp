#include "PlotPropertiesModel.h"

#include "TreeItem.h"

PlotPropertiesModel::PlotPropertiesModel(QObject *parent)
    : QAbstractItemModel(parent)
    , mRootItem{}
{
}

PlotPropertiesModel::~PlotPropertiesModel()
{
}

QModelIndex PlotPropertiesModel::index(int row, int column,
    const QModelIndex &parent) const {

    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = mRootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex PlotPropertiesModel::parent(const QModelIndex &index) const {
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == mRootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int PlotPropertiesModel::rowCount(const QModelIndex &parent) const {
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = mRootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

int PlotPropertiesModel::columnCount(const QModelIndex &parent) const {
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return mRootItem->columnCount();
}

QVariant PlotPropertiesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        if (section == 0) {
            return "Property";
        } else if (section == 1) {
            return "Value";
        }
    }

    return QVariant();
}

Qt::ItemFlags PlotPropertiesModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    if (index.column() == 1) {
        return Qt::ItemIsEditable | Qt::ItemIsUserCheckable | QAbstractItemModel::flags(index);
    } else {
        return QAbstractItemModel::flags(index);
    }
}


QVariant PlotPropertiesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
    return item->data(index.column(), role); 
}

bool PlotPropertiesModel::setData(const QModelIndex &index,
    const QVariant &value,
    int role)
{
    bool dataWasSet = false;
    if (role == Qt::EditRole || role == Qt::CheckStateRole) {
        TreeItem *item = getItem(index);
        if (role == Qt::CheckStateRole) {
            auto isChecked = value.value<Qt::CheckState>() == Qt::Checked;
            dataWasSet = item->setData(index.column(), isChecked);
        }
        else {
            dataWasSet = item->setData(index.column(), value);
        }

        if (dataWasSet)
            emit dataChanged(index, index);
    }
    return dataWasSet;
}


void PlotPropertiesModel::setRootItem(TreeItem * root)
{
    delete mRootItem;
    mRootItem = root;
}

TreeItem* PlotPropertiesModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return mRootItem;
}
