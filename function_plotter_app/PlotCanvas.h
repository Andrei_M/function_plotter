#pragma once

#include <QWidget>
#include <memory>
#include <vector>
#include <map>
#include <utility>

class QPainter;
class CanvasProperties;
namespace fun
{
class AbstractFunction;
class CanvasScene;
}

class PlotCanvas : public QWidget
{
    Q_OBJECT

public:
    PlotCanvas(QWidget *parent = nullptr);
    ~PlotCanvas();

    void setCanvasProperties(std::shared_ptr<CanvasProperties> canvasProperties);
    void setScene(std::shared_ptr<fun::CanvasScene> scene);

public slots:
    void removeSelectedFunction();

protected:
    virtual void paintEvent(QPaintEvent*) override;
    virtual void resizeEvent(QResizeEvent *) override;
    virtual void mousePressEvent(QMouseEvent *) override;
    virtual void mouseReleaseEvent(QMouseEvent *) override;
    virtual void wheelEvent(QWheelEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *) override;
    virtual void keyPressEvent(QKeyEvent *) override;

private slots:
    void sceneChanged();
    void selectedFunctionChanged();

private:
    using FunctionPtr = std::shared_ptr<fun::AbstractFunction>;

    void drawGrid(QPainter * painter, int spacing) const;
    QPointF mapFromScene(QPointF const & scenePos) const;
    QPointF mapToScene(QPointF const & viewPos) const;
    void rescaleTo(float factor);
    void updateSceneLimits();
    void computeScaleFactors(float baseValue, int nbAbove, int nbBelow);
    FunctionPtr getFunctionAt(QPointF const & pos);
    void updateFunctionsValues();
    bool isBetween(float value, float low, float high);
    static std::vector<float>::const_iterator findClosest(std::vector<float> const& values, float targetValue);
    static QPointF qpointOutside(QPointF const & a, QPointF const & b, float factor);
    static QPointF qpointBetween(QPointF const & a, QPointF const & b, float factor);


    void setSelectedFunction(FunctionPtr function);

    void resetView();

private:
    QPointF mSceneTranslate;
    std::vector<float> mSceneScaleFactors;
    int mCurrentScaleIndex;
    const float mDefaultScaleFactor = 30.0;

    std::shared_ptr<CanvasProperties> mCanvasProperties;
    std::shared_ptr<fun::CanvasScene> mScene;

    std::vector<float> mSceneXValues;
    std::vector<float> mViewXValues;

    QPoint mLastMousePos{};
    std::map<FunctionPtr, std::vector<float>> mFunctionsViewYValues;
};
