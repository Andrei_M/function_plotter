#pragma once

#include <QStyledItemDelegate>

namespace Delegates
{
class CheckBoxDelegate : public QStyledItemDelegate
{
public:
    CheckBoxDelegate(QObject * parent = nullptr)
        : QStyledItemDelegate(parent)
    { }

    virtual QWidget *CheckBoxDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &/* option */,
        const QModelIndex &/* index */) const override;

    virtual void setEditorData(QWidget *editor,
        const QModelIndex &index) const override;

    virtual void setModelData(QWidget *editor,
        QAbstractItemModel *model,
        const QModelIndex &index) const override;

    virtual void updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option,
        const QModelIndex &/* index */) const override;

    virtual void paint(QPainter * painter,
        const QStyleOptionViewItem & option,
        const QModelIndex & index) const override;
};
};

