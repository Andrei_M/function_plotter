#pragma once

#include <list>
#include <memory>
#include <iterator>
#include <QVariant>
#include <QIcon>

class PropertiesMap;

class TreeItem
{
public:
    TreeItem(std::shared_ptr<PropertiesMap> propertiesMap);
    TreeItem(TreeItem* parent, std::string const & tag, std::string const & title = "", QIcon const & icon = QIcon());
    ~TreeItem();

    QVariant data(int column, int role) const;
    bool setData(int column, const QVariant &value);

    QVariant getValue() const;
    QVariant getValueOfTag(std::string const & tag) const;

    void setTag(std::string const & tag);
    void setTitle(std::string const & title);
    void setValue(QVariant const & value);
    void setValueOfTag(std::string const & tag, QVariant const & value);

    QIcon icon() const { return mIcon; }
    void setIcon(QIcon const & icon);

    int row() const;

    void addChild(TreeItem* child);
    void removeChild(TreeItem* child);

    TreeItem* child(int row) const {
        if (mChildren.size() > row) {
            return *(std::next(mChildren.cbegin(), row));
        }
        else {
            return nullptr;
        }
    }

    TreeItem* parent() const { return mParent; }
    void setParent(TreeItem * parent);

    int childCount() { return static_cast<int>(mChildren.size()); }
    int columnCount() { return 2; }

private:
    bool hasChild(TreeItem* child);

private:
    std::string mTag;
    std::string mTitle;
    QIcon mIcon;
    TreeItem* mParent;
    std::list<TreeItem*> mChildren;
    std::shared_ptr<PropertiesMap> mPropertiesMap;
};
