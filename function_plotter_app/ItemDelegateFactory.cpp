#include "ItemDelegateFactory.h"

#include <QApplication>
#include <QCheckBox>
#include <QPainter>

namespace Delegates
{
QWidget * CheckBoxDelegate::createEditor(QWidget *parent,
    const QStyleOptionViewItem & option,
    const QModelIndex & index) const
{
    QVariant variantValue = index.data(Qt::EditRole);
    if (variantValue.type() == QVariant::Type::Bool)
    {
        return new QCheckBox(parent);
    }
    else {
        return QStyledItemDelegate::createEditor(parent, option, index);
    }

}

void CheckBoxDelegate::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    QVariant variantValue = index.data(Qt::EditRole);
    if (variantValue.type() == QVariant::Type::Bool)
    {
        bool value = index.model()->data(index, Qt::EditRole).toBool();
        QCheckBox *checkBox = static_cast<QCheckBox*>(editor);
        checkBox->setChecked(value);
    }
    else {
        return QStyledItemDelegate::setEditorData(editor, index);
    }
}

void CheckBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QVariant variantValue = index.data(Qt::EditRole);
    if (variantValue.type() == QVariant::Type::Bool)
    {
        QCheckBox *checkBox = static_cast<QCheckBox*>(editor);
        bool value = checkBox->isChecked();
        model->setData(index, value, Qt::EditRole);
    }
    else {
        return QStyledItemDelegate::setModelData(editor, model, index);
    }
}

void CheckBoxDelegate::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex & index) const
{
    QVariant variantValue = index.data(Qt::EditRole);
    if (variantValue.type() == QVariant::Type::Bool)
    {
        editor->setGeometry(option.rect);
    }
    else {
        return QStyledItemDelegate::updateEditorGeometry(editor, option, index);
    }
}

void CheckBoxDelegate::paint(QPainter * painter,
    const QStyleOptionViewItem & option,
    const QModelIndex & index) const
{
    // Draw our checkbox indicator
    QVariant variantValue = index.data(Qt::EditRole);
    if (variantValue.type() == QVariant::Type::Bool)
    {
        bool value = variantValue.toBool();
        QStyleOptionButton checkbox_indicator;

        // Set our button state to enabled
        checkbox_indicator.state |= QStyle::State_Enabled;
        checkbox_indicator.state |= (value) ? QStyle::State_On : QStyle::State_Off;

        // Get our dimensions
        checkbox_indicator.rect = QApplication::style()->subElementRect(QStyle::SE_CheckBoxIndicator, &checkbox_indicator, NULL);

        // Position our indicator
        const int x = option.rect.x();
        const int y = option.rect.center().y() - checkbox_indicator.rect.height() / 2;

        checkbox_indicator.rect.moveTo(x, y);

        if (option.state & QStyle::State_Selected) {
            painter->fillRect(option.rect, option.palette.highlight());
        }

        QApplication::style()->drawControl(QStyle::CE_CheckBox, &checkbox_indicator, painter);
    } else {
        return QStyledItemDelegate::paint(painter, option, index);
    }
}
};
