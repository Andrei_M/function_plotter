#include "FunctionPlotterWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    auto font = a.font();
    font.setPointSize(12);
    a.setFont(font);
    FunctionPlotterWindow w;
    w.show();
    return a.exec();
}
