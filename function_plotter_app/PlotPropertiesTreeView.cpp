#include "PlotPropertiesTreeView.h"

#include <QMouseEvent>

PlotPropertiesTreeView::PlotPropertiesTreeView(QWidget *parent)
    : QTreeView(parent)
{
}

PlotPropertiesTreeView::~PlotPropertiesTreeView()
{
}

void PlotPropertiesTreeView::mouseReleaseEvent(QMouseEvent *ev)
{
    bool accepted = false;
    auto const index = indexAt(ev->pos());
    if (ev->button() == Qt::LeftButton
        && model()->flags(index) & Qt::ItemIsEditable)
    {
        edit(index, AllEditTriggers, ev);
        ev->accept();
        accepted = true;
    }

    if (!accepted)
    {
        QTreeView::mouseReleaseEvent(ev);
    }
}

bool PlotPropertiesTreeView::edit(const QModelIndex &index, EditTrigger trigger, QEvent *event)
{
    return QTreeView::edit(index, trigger, event);
}
