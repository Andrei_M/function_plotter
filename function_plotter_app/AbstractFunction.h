#pragma once

#include <memory>
#include <vector>
#include <string>

namespace fun {

class Interpreter;

class AbstractFunction
{
public:
    AbstractFunction();
    ~AbstractFunction();

    std::string expression() const;
    void setInterpreter(std::shared_ptr<Interpreter> interpreter) { mInterpreter = interpreter; }
    float interpret(float xValue);
    std::vector<float> interpret(std::vector<float> const & xValues);

    float operator()(float arg);
    float operator()(std::initializer_list<std::vector<float>> args);
    template <typename... Ts>
    float operator()(Ts ...)
    {
        return 0;
    }
    

private:
    std::shared_ptr<Interpreter> mInterpreter;
};

class Function : public AbstractFunction
{
public:
   Function();

};

class PolarFunction : public AbstractFunction
{
public:
   PolarFunction();

};

}


