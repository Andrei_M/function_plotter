#include "CanvasScene.h"

#include "AbstractFunction.h"
#include <QPointF>

namespace fun
{

CanvasScene::CanvasScene()
{
}


CanvasScene::~CanvasScene()
{
}

void CanvasScene::removeFunction(std::shared_ptr<AbstractFunction> function)
{
    auto functionPos = std::find(mFunctions.begin(), mFunctions.end(), function);
    if (functionPos != mFunctions.end()) {
        mFunctions.erase(functionPos);
        emit sceneChanged();
    }
}

void CanvasScene::addFunction(std::shared_ptr<AbstractFunction> function)
{
    mFunctions.push_back(function);
    emit sceneChanged();
}

std::shared_ptr<AbstractFunction> CanvasScene::getFunctionAt(float const & x, float const & y)
{
    for (auto function : mFunctions)
    {
        float functionY = function->interpret(x);
        if (std::abs(functionY - y) < 0.7)
        {
            return function;
        }
    }
    return {};
}

void CanvasScene::selectFunction(std::shared_ptr<AbstractFunction> function)
{
    if (mSelectedFunction != function) {
        mSelectedFunction = function;
        emit selectedChanged();
    }
}
}
