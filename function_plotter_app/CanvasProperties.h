#pragma once


#include "PropertiesMap.h"
#include <memory>

class CanvasProperties
{
public:
    CanvasProperties();

    std::shared_ptr<PropertiesMap> getProperties() { return mProps; }

private:
    void initProperties();

private:
    std::shared_ptr<PropertiesMap> mProps;
};
