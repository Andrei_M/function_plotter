#include "AbstractFunction.h"

#include "Interpreter.h"

namespace fun
{

AbstractFunction::AbstractFunction()
{
}


AbstractFunction::~AbstractFunction()
{
}

float AbstractFunction::interpret(float xValue)
{
    if (!mInterpreter)
    {
        return {};
    }

    mInterpreter->setVarValue("x", xValue);
    return mInterpreter->interpret();
}

std::vector<float> AbstractFunction::interpret(std::vector<float> const & xValues)
{
    if (!mInterpreter)
    {
        return {};
    }

    std::vector<float> yValues;
    std::transform(xValues.cbegin(), xValues.cend(),
        std::back_inserter(yValues),
        [this](float xValue) -> float {
        mInterpreter->setVarValue("x", xValue);
        return mInterpreter->interpret();
    });
    return yValues;
}

std::string AbstractFunction::expression() const
{
    if (mInterpreter) {
        return mInterpreter->expression();
    }
    return "";
}

float AbstractFunction::operator()(float arg)
{
    return interpret(arg);
}

float AbstractFunction::operator()(std::initializer_list<std::vector<float>> args)
{
    return 0;
}


}

