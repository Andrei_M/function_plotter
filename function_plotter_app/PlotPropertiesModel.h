#pragma once

#include <QAbstractItemModel>

class TreeItem;

class PlotPropertiesModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    PlotPropertiesModel(QObject *parent);
    ~PlotPropertiesModel();

    virtual QModelIndex index(int row, int column,
                  const QModelIndex &parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex &index) const override;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    void setRootItem(TreeItem * root);

private:
    TreeItem* getItem(const QModelIndex &index) const;

private:
    TreeItem * mRootItem;
};
