#include "PlotCanvas.h"

#include <assert.h>

#include <QApplication>
#include <QPainter>
#include <QMouseEvent>
#include <QAction>
#include <QMenu>

#include "Interpreter.h"
#include "CanvasProperties.h"
#include "CanvasScene.h"
#include "AbstractFunction.h"

#include <QDebug>

PlotCanvas::PlotCanvas(QWidget *parent)
    : QWidget(parent)
    , mScene{nullptr}
{
    // allow the widget to grab the focus
    // http://doc.qt.io/qt-5/qt.html#FocusPolicy-enum
    setFocusPolicy(Qt::StrongFocus);
    
    // init the scene
    computeScaleFactors(1.0, 300, 300);
    rescaleTo(mDefaultScaleFactor);
    updateSceneLimits();
    updateFunctionsValues();
}

PlotCanvas::~PlotCanvas()
{
}

void PlotCanvas::computeScaleFactors(float baseValue, int nbAbove, int nbBelow)
{
    mSceneScaleFactors.clear();
    float aboveStep = 1.1;
    float belowStep = 0.9;
    nbBelow = std::max(0, nbBelow);
    float currentValue = baseValue;
    for (int i = 0; i < nbBelow; ++i)
    {
        currentValue *= belowStep;
        mSceneScaleFactors.insert(mSceneScaleFactors.begin(), currentValue);
    }

    currentValue = baseValue;
    mSceneScaleFactors.push_back(currentValue);

    nbAbove = std::max(0, nbAbove);
    for (int i = 0; i < nbAbove; ++i)
    {
        currentValue *= aboveStep;
        mSceneScaleFactors.push_back(currentValue);
    }
}

void PlotCanvas::rescaleTo(float factor)
{
    auto pos = findClosest(mSceneScaleFactors, factor);
    if (pos != mSceneScaleFactors.cend()) {
        mCurrentScaleIndex = std::distance(mSceneScaleFactors.cbegin(), pos);
    }
    else {
        mCurrentScaleIndex = 0;
    }
}


void PlotCanvas::setCanvasProperties(std::shared_ptr<CanvasProperties> canvasProperties)
{
    mCanvasProperties = canvasProperties;
    update();
}

void PlotCanvas::setScene(std::shared_ptr<fun::CanvasScene> scene)
{
    mScene = scene;
    connect(mScene.get(),
        &fun::CanvasScene::sceneChanged,
        this,
        &PlotCanvas::sceneChanged);
    connect(mScene.get(),
        &fun::CanvasScene::selectedChanged,
        this,
        &PlotCanvas::selectedFunctionChanged);
    update();
}

void PlotCanvas::drawGrid(QPainter * painter, int spacing) const
{
    painter->save();

    painter->setPen(Qt::darkGray);

    auto left = rect().left();
    auto right = rect().right();
    auto top = rect().top();
    auto bottom = rect().bottom();

    for (int step = left; step <= right; step += spacing) {
        painter->drawLine(step, top, step, bottom);
    }

    for (int step = top; step <= bottom; step += spacing) {
        painter->drawLine(left, step, right, step);
    }

    painter->restore();
}

void PlotCanvas::paintEvent(QPaintEvent*)
{
    auto props = mCanvasProperties->getProperties();
    auto drawRect = rect();
    auto sceneOriginPos = mapFromScene(QPointF{ 0,0 });
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // draw the background
    painter.fillRect(drawRect, Qt::lightGray);
    auto canvasColor = props->getColor("canvas color");
    painter.fillRect(drawRect, canvasColor);

    // draw the grid
    if (props->getBool("grid visible"))
    {
        // get the axes
        float axisY = sceneOriginPos.y();
        float axisX = sceneOriginPos.x();
        const QLineF vertAxisLine = QLineF(axisX, 0, axisX, height());
        const QLineF horizAxisLine = QLineF(0, axisY, width(), axisY);

        painter.save();
        QPen pen = QPen{};
        pen.setColor(Qt::darkGray);
        pen.setStyle(Qt::DotLine);
        pen.setWidth(1);
        painter.setPen(pen);

        float gridSpacing = std::abs(mSceneScaleFactors[mCurrentScaleIndex]);
        // vertical grid lines
        QVector<QLineF> gridLines;
        QLineF vertGridLine = vertAxisLine;
        while (vertGridLine.x1() >= 0) {
            gridLines << vertGridLine;
            vertGridLine.translate(-gridSpacing, 0);
        }

        vertGridLine = vertAxisLine;
        while (vertGridLine.x1() <= width()) {
            gridLines << vertGridLine;
            vertGridLine.translate(+gridSpacing, 0);
        }

        // horizontal grid lines
        QLineF horizGridLine = horizAxisLine;
        while (horizGridLine.y1() >= 0) {
            gridLines << horizGridLine;
            horizGridLine.translate(0, -gridSpacing);
        }

        horizGridLine = horizAxisLine;
        while (horizGridLine.y1() <= height()) {
            gridLines << horizGridLine;
            horizGridLine.translate(0, +gridSpacing);
        }

        painter.drawLines(gridLines);
    }

    // draw axes
    if (props->getBool("axes visible"))
    {
        painter.save();
        auto axisPen = QPen{};
        axisPen.setColor(Qt::black);
        axisPen.setWidth(props->getInt("axes width"));
        painter.setPen(axisPen);
        // horizontal axis
        float axisY = sceneOriginPos.y();
        const QLineF horizAxisLine = QLineF(0, axisY, width(), axisY);
        painter.drawLine(horizAxisLine);

        // vertical axis
        float axisX = sceneOriginPos.x();
        const QLineF vertAxisLine = QLineF(axisX, 0, axisX, height());
        painter.drawLine(vertAxisLine);
        painter.restore();
    }

    QColor penColor;
    QPen pen;
    int penWidth;
    // draw the functions in the scene
    for (auto function : mScene->functions())
    {
        if (function == mScene->getSelectedFunction()) {
            penColor = Qt::green;
            penWidth = 4;
        } else {
            penColor = Qt::red;
            penWidth = 2;
        }
        pen.setWidth(penWidth);
        pen.setColor(penColor);
        painter.setPen(pen);

        auto functionViewYValues = mFunctionsViewYValues[function];

        const size_t nbPoints = mViewXValues.size();
        for (size_t i = 0; i < nbPoints - 1; ++i) {
            auto firstPoint = QPointF{ mViewXValues[i], functionViewYValues[i] };
            auto secondPoint = QPointF{ mViewXValues[i + 1], functionViewYValues[i + 1] };
            painter.drawLine(firstPoint, secondPoint);
        }

    }

    // draw scene origin symbol
    if (props->getBool("origin visible")) {
        painter.save();
        painter.setBrush(Qt::darkBlue);
        painter.drawEllipse(sceneOriginPos, 5, 5);
        painter.restore();
    }

    // diagnostics
    bool drawDiagnosticsOnWidget = true;
    if (drawDiagnosticsOnWidget)
    {
        QStringList diagnosticsMessages;

        float scaleX = mSceneScaleFactors[mCurrentScaleIndex];
        float scaleY = scaleX;
        diagnosticsMessages << QString("zoom: (%1,%2)").arg(scaleX).arg(scaleY);
        diagnosticsMessages << QString("translate: (%1,%2)").arg(mSceneTranslate.x()).arg(mSceneTranslate.y());
        if (mScene->getSelectedFunction()) {
            diagnosticsMessages << QString("function: \"%1\"").arg(QString::fromStdString(mScene->getSelectedFunction()->expression()));
        }

        QFontMetrics fontMetrics(QApplication::font(), this);
        auto textHeight = fontMetrics.height() * diagnosticsMessages.count();
        auto messagesRect = QRect(0, 0, width(), textHeight);
        messagesRect.moveBottomLeft(drawRect.bottomLeft() + QPoint{ 2, -2 });
        painter.setPen(Qt::darkMagenta);
        painter.drawText(messagesRect, diagnosticsMessages.join('\n'));
    }
}

QPointF PlotCanvas::mapToScene(QPointF const & viewPos) const
{
    float scaleX = mSceneScaleFactors[mCurrentScaleIndex];
    float scaleY = scaleX;
    return {
        (viewPos.x() - mSceneTranslate.x()) /  scaleX ,
        (viewPos.y() - mSceneTranslate.y()) / -scaleY 
    };
}

QPointF PlotCanvas::mapFromScene(QPointF const & scenePos) const
{
    float scaleX = mSceneScaleFactors[mCurrentScaleIndex];
    float scaleY = scaleX;
    return {
        scenePos.x()* scaleX + mSceneTranslate.x(),
        scenePos.y()*-scaleY + mSceneTranslate.y()
    };
}

void PlotCanvas::resizeEvent(QResizeEvent*)
{
    if (mSceneTranslate.isNull())
    {
        mSceneTranslate = { float(width())/2, height()-float(height())/4 };
    }

    updateSceneLimits();
    updateFunctionsValues();
}

void PlotCanvas::updateSceneLimits()
{
    // update the scene limits
    auto drawRect = rect();

    auto sceneTL = mapToScene(drawRect.topLeft());
    auto sceneBR = mapToScene(drawRect.bottomRight());
    float minX = sceneTL.x();
    float maxX = sceneBR.x();
    float nbPoints = 1000;
    float xDiff = (maxX - minX) / nbPoints;
    mSceneXValues.clear();
    for (float currX = minX; currX <= maxX; currX += xDiff)
    {
        mSceneXValues.push_back(currX);
    }

    mViewXValues.clear();
    std::transform(mSceneXValues.cbegin(), mSceneXValues.cend(),
        std::back_inserter(mViewXValues),
        [this](float sceneXValue) -> float { return mapFromScene({ sceneXValue, 0 }).x(); }
    );
}

void PlotCanvas::updateFunctionsValues()
{
    if (mScene) {
        for (auto function : mScene->functions()) {
            auto functionYValues = function->interpret(mSceneXValues);
            std::vector<float> viewYValues;
            std::transform(functionYValues.cbegin(), functionYValues.cend(),
                std::back_inserter(viewYValues),
                [this](float yValue) -> float { return mapFromScene({ 0, yValue }).y(); }
            );
            mFunctionsViewYValues[function] = viewYValues;
        }
    }
}

void PlotCanvas::setSelectedFunction(FunctionPtr function)
{
    mScene->selectFunction(function);
}

void PlotCanvas::keyPressEvent(QKeyEvent * ev)
{
    if (ev->key() == Qt::Key_R) {
        resetView();
        ev->accept();
    } else {
        QWidget::keyPressEvent(ev);
    }
}

void PlotCanvas::mousePressEvent(QMouseEvent * ev)
{
    auto viewPos = ev->pos();
    if (ev->button() & Qt::LeftButton) {
        setSelectedFunction(getFunctionAt(viewPos));
    }

    if (ev->button() & Qt::MiddleButton) {
        mLastMousePos = viewPos;
        setCursor(Qt::OpenHandCursor);
    }

    update();
}

void PlotCanvas::mouseReleaseEvent(QMouseEvent * ev)
{
    if (ev->button() & Qt::MiddleButton)
    {
        unsetCursor();
    }
}

PlotCanvas::FunctionPtr PlotCanvas::getFunctionAt(QPointF const & viewPos)
{
    const int maxDist = 20; // max distance in pixels from given position
    const int minX = viewPos.x() - maxDist;
    const int maxX = viewPos.x() + maxDist;
    QPointF p1, p2 = {};
    for (int x = minX; x < maxX - 1; ++x) {
        p1.setX(mapToScene({ (float)x , 0 }).x());
        p2.setX(mapToScene({ (float)x + 1 , 0 }).x());
        for (auto function : mScene->functions()) {
            p1.setY(function->interpret(p1.x()));
            p2.setY(function->interpret(p2.x()));

            // distance from point to line segment
            // https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
            auto viewP1 = mapFromScene(p1);
            auto viewP2 = mapFromScene(p2);
            QLineF functionSegment = { viewP1, viewP2 };
            if (functionSegment.length() == 0) {
                QLineF distline = { viewPos, viewP1 };
                if (distline.length() <= maxDist) {
                    return function;
                }
            } else {
                const float t = std::max(0.0, std::min(1.0, QPointF::dotProduct(viewPos - viewP1, viewP2 - viewP1) / (functionSegment.length()*functionSegment.length())));
                const QPointF projection = viewP1 + t * (viewP2 - viewP1);
                const QLineF distLine = QLineF{ viewPos, projection };
                if (distLine.length() <= maxDist) {
                    return function;
                }
            }
        }
    }
    return FunctionPtr{};
}

bool PlotCanvas::isBetween(float value, float low, float high)
{
    if (low > high) {
        std::swap(low, high);
    }
    return low <= value && high >= value;
}

std::vector<float>::const_iterator PlotCanvas::findClosest(std::vector<float> const & values, float targetValue)
{
    return std::lower_bound(values.cbegin(), values.cend(), targetValue);
}

void PlotCanvas::wheelEvent(QWheelEvent *event)
{
    QPoint numDegrees = event->angleDelta() / 8;

    if (!numDegrees.isNull()) {
        QPoint numSteps = numDegrees / 15;
        // change the scale factor
        {
            mCurrentScaleIndex += numSteps.y();
            mCurrentScaleIndex = std::max(mCurrentScaleIndex, 0);
            mCurrentScaleIndex = std::min(mCurrentScaleIndex, (int)mSceneScaleFactors.size() - 1);
        }

        // reposition the scene origin
        {
            auto sceneOriginView = mapFromScene(QPointF{ 0, 0 });
            auto mousePosition = event->posF();
            bool zoomOut = numSteps.y() > 0;
            auto newSceneTranslate = zoomOut
                ? qpointOutside(mousePosition, sceneOriginView, 8) // move origin away from cursor
                : qpointBetween(mousePosition, sceneOriginView, 8); // move origin towards the cursor
            mSceneTranslate = newSceneTranslate;
        }

        // recompute the scene limits and update the view
        updateSceneLimits();
        updateFunctionsValues();
        update();
    }

    event->accept();
}

void PlotCanvas::mouseMoveEvent(QMouseEvent * ev)
{
    if (ev->buttons() & Qt::MiddleButton) {
        setCursor(Qt::ClosedHandCursor);
        auto pos = ev->pos();

        auto posDiff = pos - mLastMousePos;
        mSceneTranslate += posDiff;
        mLastMousePos = ev->pos();

        updateSceneLimits();
        updateFunctionsValues();
        update();
    }
}

void PlotCanvas::mouseDoubleClickEvent(QMouseEvent * ev)
{
    if (ev->buttons() & Qt::LeftButton)
    {
        resetView();
    }
}

void PlotCanvas::resetView()
{
    // reposition the scene origin
    mSceneTranslate = { float(width()) / 2, float(height()) / 2 };

    // scale to default value
    rescaleTo(mDefaultScaleFactor);

    // recompute the scene limits and update the view
    updateSceneLimits();
    updateFunctionsValues();
    update();
}

void PlotCanvas::sceneChanged()
{
    updateFunctionsValues();
    update();
}

void PlotCanvas::selectedFunctionChanged()
{
    update();
}

void PlotCanvas::removeSelectedFunction()
{
    mScene->removeFunction(mScene->getSelectedFunction());
}

QPointF PlotCanvas::qpointBetween(QPointF const & a, QPointF const & b, float factor)
{
    auto const af = 1/factor;
    auto const bf = 1 - af;
    return { af*a + bf*b };
}


QPointF PlotCanvas::qpointOutside(QPointF const & a, QPointF const & b, float factor)
{
    auto const a1 = b + (b-a);
    return qpointBetween(a1, b, factor);
}
