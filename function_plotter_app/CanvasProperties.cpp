#include "CanvasProperties.h"

#include <QVariant>
#include <QColor>

CanvasProperties::CanvasProperties()
{
    initProperties();
}

void CanvasProperties::initProperties()
{
    mProps = std::make_shared<PropertiesMap>();
    
    (*mProps)["canvas color"] = QVariant::fromValue<QColor>(Qt::lightGray);

    (*mProps)["origin visible"] = true;

    // grid 
    (*mProps)["grid visible"] = false;
    (*mProps)["grid color"] = QVariant::fromValue<QColor>(Qt::red);
    (*mProps)["grid spacing"] = 50;

    // axes
    (*mProps)["axes visible"] = true;
    (*mProps)["axes width"] = 3;
    (*mProps)["axes color"] = QVariant::fromValue<QColor>(Qt::black);
    (*mProps)["large ticks visible"] = true;
    (*mProps)["small ticks visible"] = true;
    (*mProps)["numbers visible"] = true;
    (*mProps)["numbers color"] = QVariant::fromValue<QColor>(Qt::black);
}