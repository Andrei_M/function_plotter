#pragma once

#include <map>
#include <QVariant>
#include <QColor>

class PropertiesMap
{
public:
    QVariant const & getProperty(std::string const & propertyName)
    {
        return mProps[propertyName];
    }

    int getInt(std::string const & propertyName)
    {
        if (mProps[propertyName].type() != QVariant::Int) {
            throw std::exception("Attempt to retrieve int value from non-matching variant");
        }
        return mProps[propertyName].toInt();
    }

    float getFloat(std::string const & propertyName)
    {
       if (mProps[propertyName].type() != QVariant::Double) {
           throw std::exception("Attempt to retrieve float value from non-matching variant");
       }
       return mProps[propertyName].toFloat();
    }

    std::string getString(std::string const & propertyName)
    {
        if (mProps[propertyName].type() != QVariant::String) {
            throw std::exception("attempt to retrieve string type from non-matching variant");
        }
        return mProps[propertyName].toString().toStdString();
    }

    bool getBool(std::string const & propertyName)
    {
        if (mProps[propertyName].type() != QVariant::Bool) {
            throw std::exception("attempt to retrieve bool type from non-matching variant");
        }
        return mProps[propertyName].toBool();
    }

    QColor getColor(std::string const & propertyName)
    {
        if (mProps[propertyName].type() != QVariant::Color) {
            throw std::exception("attempt to retrieve bool type from non-matching variant");
        }
        return mProps[propertyName].value<QColor>();
    }

    void setProperty(std::string const & propertyName, QVariant const & propertyValue)
    {
        mProps[propertyName] = propertyValue;
    }

    QVariant& operator[](std::string const & propertyName)
    {
        return mProps[propertyName];
    }

    QVariant const & operator[](std::string const & propertyName) const
    {
        auto pos = mProps.find(propertyName);
        return pos->second;
    }

private:
    std::map<std::string, QVariant> mProps;
};

