#pragma once

#include "function_parser_global.h"
#include <QVariant>

namespace fun {
struct FUNCTION_PARSER_EXPORT Token
{
    enum Type
    {
        NONE,
        INTEGER,
        REAL,
        PLUS,
        MINUS,
        MUL,
        DIV,
        POW,
        LPAREN,
        RPAREN,
        ID, // variable ("x"), function ("sin(x)")
    };

    Type mType;
    QVariant mValue;

    Token(Type type, QVariant const & value)
        : mType(type)
        , mValue(value)
    { }

    Token(const Token & other)
        : mType(other.mType)
        , mValue(other.mValue)
    { }

    Token(const Token && other)
        : mType(std::move(other.mType))
        , mValue(std::move(other.mValue))
    { }

    friend void swap(Token & first, Token & second)
    {
        using std::swap;
        swap(first.mType, second.mType);
        swap(first.mValue, second.mValue);
    }

    Token & operator=(Token other)
    {
        swap(*this, other);
        return *this;
    }

    static Token makeInvalid() { return Token(NONE, ""); }
};
}
