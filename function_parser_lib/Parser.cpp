#include "Parser.h"

#include <cmath>

namespace fun {
Parser::Parser()
    : mData(std::make_shared<ParserImpl>())
{

}

Parser::Parser(Lexer const & lexer)
    : mData(std::make_shared<ParserImpl>(lexer))
{

}

ParserImpl::ParserImpl()
    : ParserImpl(Lexer())
{ }

ParserImpl::ParserImpl(fun::Lexer const & lexer)
    : mLexer(lexer)
    , mCurrentToken(Token::makeInvalid())
{
    mBuiltInFunctions["sin"] = &sin;
    mBuiltInFunctions["cos"] = &cos;
    mBuiltInFunctions["tan"] = &tan;
    updateToken();
}

void ParserImpl::addBuiltInFunction(std::string name, FunctionNode::SingleParamFunctionPtr function)
{
    mBuiltInFunctions[name] = function;
}


void ParserImpl::updateToken()
{
    mCurrentToken = mLexer.getNextToken();
}

void ParserImpl::eat(Token::Type const & tokenType)
{
    if (mCurrentToken.mType == tokenType) {
        updateToken();
    }
    else {
        throw std::exception("Invalid syntax");
    }
}

std::shared_ptr<Node> ParserImpl::constant()
{
    Token token = mCurrentToken;
    if (token.mType == Token::PLUS) {
        eat(Token::PLUS);
        auto node = std::make_shared<UnaryOp>(token, constant());
        return node;
    }
    else if (token.mType == Token::MINUS) {
        eat(Token::MINUS);
        auto node = std::make_shared<UnaryOp>(token, constant());
        return node;
    }
    else if (token.mType == Token::INTEGER) {
        eat(Token::INTEGER);
        auto node = std::make_shared<Num>(token);
        return node;
    }
    else if (token.mType == Token::REAL) {
        eat(Token::REAL);
        auto node = std::make_shared<Num>(token);
        return node;
    }
    else if (token.mType == Token::LPAREN) {
        eat(Token::LPAREN);
        auto node = expr();
        eat(Token::RPAREN);
        return node;
    }
    else if (token.mType == Token::ID) {
        std::string functionName = token.mValue.toString().toStdString();
        auto const functionIt = mBuiltInFunctions.find(functionName);
        if (functionIt != mBuiltInFunctions.end()) {
            eat(Token::ID);
            eat(Token::LPAREN);
            auto arg = expr();
            eat(Token::RPAREN);
            auto node = std::make_shared<FunctionNode>(token, functionIt->second, arg);
            return node;
        }
        else {
            eat(Token::ID);
            auto node = std::make_shared<Var>(token);
            return node;
        }
    }
    return std::shared_ptr<Node>(std::make_shared<Node>());
}

std::shared_ptr<Node> ParserImpl::factor()
{
    auto node = constant();

    Token token = Token::makeInvalid();
    while (mCurrentToken.mType == Token::POW) {
        token = mCurrentToken;
        eat(Token::POW);
        auto exponent = factor();
        node = std::make_shared<BinOp>(node, token, exponent);
    }

    // can read expressions like 3x+2
    if (mCurrentToken.mType == Token::ID) {
        auto idToken = factor();
        auto mulToken = Token(Token::MUL, '*');
        node = std::make_shared<BinOp>(node, mulToken, idToken);
    }

    // can read expressions like 4(5-2) or 1/x(3+5)
    // but what about functions like "sin(x)" ? it should not be interpreted
    // as "sin * x"
    while (mCurrentToken.mType == Token::LPAREN) {
        auto lparenToken = factor();
        auto mulToken = Token(Token::MUL, '*');
        node = std::make_shared<BinOp>(node, mulToken, lparenToken);
    }

    return node;
}

std::shared_ptr<Node> ParserImpl::term()
{
    auto node = factor();
    while (mCurrentToken.mType == Token::MUL
        || mCurrentToken.mType == Token::DIV) {
        auto token = mCurrentToken;
        if (token.mType == Token::MUL) {
            eat(Token::MUL);
        }
        else if (token.mType == Token::DIV) {
            eat(Token::DIV);
        }

        auto right = factor();
        node = std::make_shared<BinOp>(node, token, right);
    }

    return node;
}

std::shared_ptr<Node> ParserImpl::expr()
{
    auto node = term();

    Token token = Token::makeInvalid();
    while (mCurrentToken.mType == Token::PLUS
        || mCurrentToken.mType == Token::MINUS)
    {
        Token token = mCurrentToken;
        if (token.mType == Token::PLUS) {
            eat(Token::PLUS);
        }
        else if (token.mType == Token::MINUS) {
            eat(Token::MINUS);
        }

        node = std::make_shared<BinOp>(node, token, term());
    }

    return node;
}
}
