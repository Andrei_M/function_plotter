#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(FUNCTION_PARSER_LIB)
#  define FUNCTION_PARSER_EXPORT Q_DECL_EXPORT
# else
#  define FUNCTION_PARSER_EXPORT Q_DECL_IMPORT
# endif
#else
# define FUNCTION_PARSER_EXPORT
#endif
