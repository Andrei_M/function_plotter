TEMPLATE = lib
TARGET = function_parser
CONFIG += shared
DEFINES += FUNCTION_PARSER_LIB

HEADERS = AST.h \
    function_parser_global.h \
    Interpreter.h \
    Lexer.h \
    Parser.h \
    Token.h

SOURCES = AST.cpp \
    Interpreter.cpp \
    Lexer.cpp \
    Parser.cpp
