#pragma once

#include "function_parser_global.h"

#include <cctype>
#include <string>
#include <memory>

#include "Token.h"

namespace fun {

class LexerImpl
{
    friend class Lexer;
public:
    LexerImpl(std::string const & expression);

private:
    void advance();
    void updateCurrent();
    bool currentIsValid() const { return mCurrent != '\0'; }
    bool currentIsSpace() const { return std::isspace(mCurrent); }
    bool currentIsDigit() const { return std::isdigit(mCurrent); }
    bool currentIsDot() const { return mCurrent == '.'; }
    bool currentIsId() const { return std::isalnum(mCurrent) || mCurrent == '_'; }
    void skipWhitespace();
    Token parseNumber();
    Token parseId();
    Token getNextToken();
    inline void error() const;

private:
    int mPos;
    std::string mInputString;
    char mCurrent;
};

class FUNCTION_PARSER_EXPORT Lexer
{
public:
    Lexer();
    Lexer(std::string const & expression);

    Token getNextToken() { return mData->getNextToken(); }

private:
    std::shared_ptr<LexerImpl> mData;
};
}
