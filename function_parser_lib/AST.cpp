#include "AST.h"
#include <iostream>


namespace fun {
Num::Num(Token const & token)
    : Node(NUM)
    , mToken(token)
{
}

float Num::value() const {
    return mToken.mValue.toFloat();
}

Var::Var(Token const & token)
    : Node(ID)
    , mToken(token)
{ }

std::string Var::name() const {
    return mToken.mValue.toString().toStdString();
}

FunctionNode::FunctionNode(Token const & token, SingleParamFunctionPtr function, std::shared_ptr<Node> arg)
    : Node(ID)
    , mToken(token)
    , mFunction(function)
    , mArg(arg)
{ }

std::string FunctionNode::name() const {
    return mToken.mValue.toString().toStdString();
}
}