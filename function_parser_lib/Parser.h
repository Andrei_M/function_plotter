#pragma once

#include "function_parser_global.h"

#include <map>
#include <memory>

#include "Lexer.h"
#include "AST.h"

namespace fun {
class ParserImpl
{
    friend class Parser;

public:
    ParserImpl();
    ParserImpl(fun::Lexer const & lexer);

private:
    void eat(Token::Type const & tokenType);
    void updateToken();
    std::shared_ptr<Node> constant();
    std::shared_ptr<Node> factor();
    std::shared_ptr<Node> term();
    std::shared_ptr<Node> expr();
    void addBuiltInFunction(std::string name, FunctionNode::SingleParamFunctionPtr);

private:
    Lexer mLexer;
    Token mCurrentToken;
    std::map<std::string, FunctionNode::SingleParamFunctionPtr> mBuiltInFunctions;
};

class FUNCTION_PARSER_EXPORT Parser {
public:
    Parser();
    Parser(Lexer const & lexer);

    std::shared_ptr<Node> parse() { return mData->expr(); }
    void addBuiltInFunction(std::string name, FunctionNode::SingleParamFunctionPtr function) { mData->addBuiltInFunction(name, function); }

private:
    std::shared_ptr<ParserImpl> mData;
};
}
