
#include "Interpreter.h"


namespace fun {
void Interpreter::setVarValue(std::string const & varName, float varValue)
{
    mIdTable[varName] = varValue;
}

float Interpreter::visit(std::shared_ptr<Node> const & node)
{
    switch (node->type()) {
    case NONE:
        break;
    case NUM:
        return visitNum(std::dynamic_pointer_cast<Num>(node));
    case BIN_OP:
        return visitBinOp(std::dynamic_pointer_cast<BinOp>(node));
    case UNARY_OP:
        return visitUnaryOp(std::dynamic_pointer_cast<UnaryOp>(node));
    case ID:
    {
        auto varId = std::dynamic_pointer_cast<Var>(node);
        if (varId) {
            return visitId(varId); 
        }
        auto functionId = std::dynamic_pointer_cast<FunctionNode>(node);
        if (functionId) {
            return visitFunction(functionId);
        }
        throw std::exception("Unknown id type");
    }
    default:
        throw std::exception("Unsupported AST node type");
    }
    return 0;
}

float Interpreter::visitBinOp(std::shared_ptr<BinOp> const & binOpNode)
{
    switch (binOpNode->tokenType()) {
    case Token::PLUS:
        return visit(binOpNode->left()) + visit(binOpNode->right());

    case Token::MINUS:
        return visit(binOpNode->left()) - visit(binOpNode->right());

    case Token::MUL:
        return visit(binOpNode->left()) * visit(binOpNode->right());

    case Token::DIV:
        return visit(binOpNode->left()) / visit(binOpNode->right());

    case Token::POW:
    {
        auto base = visit(binOpNode->left());
        auto exponent = visit(binOpNode->right());
        return std::powf(base, exponent);
    }
    default:
        throw std::exception("Unsupported operation");
    }
    return 0;
}

float Interpreter::visitNum(std::shared_ptr<Num> const & numNode)
{
    return numNode->value();
}

float Interpreter::visitId(std::shared_ptr<Var> const & idNode)
{
    auto idPos = mIdTable.find(idNode->name());
    if (idPos != mIdTable.cend()) {
        return idPos->second;
    }

    auto builtInIdPos = mBuiltInIdTable.find(idNode->name());
    if (builtInIdPos != mBuiltInIdTable.cend()) {
        return builtInIdPos->second;
    }

    throw std::exception(std::string("No id named \"").append(idNode->name()).append("\" has been defined").c_str());
}

float Interpreter::visitFunction(std::shared_ptr<FunctionNode> const & functionNode)
{
    float argValue = visit(functionNode->arg());
    float functionValue = functionNode->function()(argValue);
    return functionValue;
}


float Interpreter::visitUnaryOp(std::shared_ptr<UnaryOp> const & unaryOpNode)
{
    if (unaryOpNode->tokenType() == Token::MINUS) {
        return -visit(unaryOpNode->expr());
    }
    return visit(unaryOpNode->expr());
}

std::string Interpreter::expression() const
{
    return mCachedExpression;
}
}
