#pragma once


#include "function_parser_global.h"

#include <memory>
#include <string>
#include "AST.h"
#include "Parser.h"


namespace fun {
class FUNCTION_PARSER_EXPORT Interpreter : public NodeVisitor
{
public:
    Interpreter(std::shared_ptr<Node> const & expressionAST)
        : NodeVisitor()
        , mExpressionAST(expressionAST)
    { 
        mBuiltInIdTable["pi"] = 3.14159f;
    }

    void setVarValue(std::string const & varName, float varValue);

    virtual float visit(std::shared_ptr<Node> const & node) override;
    float interpret() {
        return visit(mExpressionAST);
    }

    // Not used by the interpreter itself, and may not be accurate.
    // Just a quick hack to have a link between the AST and the original expression
    // without having to recompute the expression from the AST (which should be the
    // way to obtian the original expression)
    void setExpression(std::string const & expression) { mCachedExpression = expression; }
    std::string expression() const;

private:
    float visitBinOp(std::shared_ptr<BinOp> const & binOpNode);
    float visitNum(std::shared_ptr<Num> const & numNode);
    float visitUnaryOp(std::shared_ptr<UnaryOp> const & unaryOpNode);
    float visitId(std::shared_ptr<Var> const & idNode);
    float visitFunction(std::shared_ptr<FunctionNode> const & functionNode);

private:
    std::map<std::string, float> mIdTable;
    std::map<std::string, float> mBuiltInIdTable;
    std::shared_ptr<Node> mExpressionAST;
    std::string mCachedExpression; // original expression, before AST. Can be reconstructed from AST
};
}
