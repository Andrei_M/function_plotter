#include "Lexer.h"
#include <sstream>

namespace fun {

LexerImpl::LexerImpl(std::string const & expression)
    : mPos(0)
    , mInputString(expression)
{
    updateCurrent();
}

void LexerImpl::advance()
{
    ++mPos;
    updateCurrent();
}

void LexerImpl::updateCurrent()
{
    mCurrent = mPos < mInputString.length()
        ? mInputString.at(mPos)
        : '\0';
}

void LexerImpl::skipWhitespace()
{
    while (currentIsValid() && currentIsSpace())
    {
        advance();
    }
}

Token LexerImpl::parseNumber()
{
    Token result = Token::makeInvalid();
    std::ostringstream resultStream;
    while (currentIsValid() && currentIsDigit())
    {
        resultStream << mCurrent;
        advance();
    }

    if (currentIsDot()) {
        resultStream << mCurrent;
        advance();

        while (currentIsValid() && currentIsDigit())
        {
            resultStream << mCurrent;
            advance();
        }

        result = Token(Token::REAL, std::stof(resultStream.str()));
    }
    else
    {
        result = Token(Token::INTEGER, std::stoi(resultStream.str()));
    }
    return result;
}

Token LexerImpl::parseId()
{
    std::ostringstream result;
    while (currentIsValid() && currentIsId()) {
        result << mCurrent;
        advance();
    }

    Token token = Token(Token::ID, QString::fromStdString(result.str()));
    return token;
}

Token LexerImpl::getNextToken()
{
    while (currentIsValid())
    {
        if (currentIsSpace()) {
            skipWhitespace();
            continue;
        }

        if (currentIsDigit() || currentIsDot()) {
            return parseNumber();
        }

        if (currentIsId()) {
            return parseId();
        }

        if (mCurrent == '+') {
            advance();
            return Token(Token::PLUS, "+");
        }

        if (mCurrent == '-') {
            advance();
            return Token(Token::MINUS, "-");
        }

        if (mCurrent == '*') {
            advance();
            return Token(Token::MUL, "*");
        }

        if (mCurrent == '^') {
            advance();
            return Token(Token::POW, "^");
        }

        if (mCurrent == '/') {
            advance();
            return Token(Token::DIV, "/");
        }

        if (mCurrent == '(') {
            advance();
            return Token(Token::LPAREN, "(");
        }

        if (mCurrent == ')') {
            advance();
            return Token(Token::RPAREN, ")");
        }

        error();
    }

    return Token(Token::NONE, "");
}

void LexerImpl::error() const
{
    throw std::exception("Invalid character: " + mCurrent);
}

Lexer::Lexer()
    : Lexer("")
{
}

Lexer::Lexer(std::string const & expression)
    : mData(std::make_shared<LexerImpl>(expression))
{

}
}
