#pragma once

#include "function_parser_global.h"

#include <memory>
#include <string>
#include "Token.h"

namespace fun {
enum NodeType
{
    NONE,
    NUM,
    BIN_OP,
    UNARY_OP,
    ID,
};

class FUNCTION_PARSER_EXPORT Node {
public:
    Node() { *this = makeInvalid(); }
    Node(NodeType const & type) : mType(type) { }
    virtual ~Node() { }

    NodeType type() const { return mType; }
    static Node makeInvalid() { return Node(NONE); }

private:
    NodeType mType;
};

class FUNCTION_PARSER_EXPORT NodeVisitor
{
public:
    NodeVisitor() { }
    virtual ~NodeVisitor() { }
    virtual float visit(std::shared_ptr<Node> const & node) = 0;
};

class FUNCTION_PARSER_EXPORT BinOp : public Node
{
public:
    BinOp(std::shared_ptr<Node> const & left, Token const & op, std::shared_ptr<Node> const & right)
        : Node(NodeType::BIN_OP)
        , mLeft(left)
        , mOperatorToken(op)
        , mRight(right)
    { }

    Token::Type tokenType() const { return mOperatorToken.mType; }
    std::shared_ptr<Node> left() const { return mLeft; }
    std::shared_ptr<Node> right() const { return mRight; }

private:
    std::shared_ptr<Node> mLeft;
    Token mOperatorToken;
    std::shared_ptr<Node> mRight;
};

class FUNCTION_PARSER_EXPORT UnaryOp : public Node
{
public:
    UnaryOp(Token const & op, std::shared_ptr<Node> const & expr)
        : Node(NodeType::UNARY_OP)
        , mOperatorToken(op)
        , mExpr(expr)
    { }

    Token::Type tokenType() const { return mOperatorToken.mType; }
    std::shared_ptr<Node> expr() const { return mExpr; }

private:
    Token mOperatorToken;
    std::shared_ptr<Node> mExpr;
};

class FUNCTION_PARSER_EXPORT Num : public Node
{
public:
    Num(Token const & token);
    float value() const;

private:
    Num(Num const & other) = delete;

private:
    Token mToken;
};

class FUNCTION_PARSER_EXPORT Var : public Node
{
public:
    Var(Token const & token);
    std::string name() const;

private:
    Token mToken;
};

class FUNCTION_PARSER_EXPORT FunctionNode : public Node
{
public:
    using SingleParamFunctionPtr = float(*)(float);
    FunctionNode(Token const & token, SingleParamFunctionPtr function, std::shared_ptr<Node> arg);
    std::string name() const;
    std::shared_ptr<Node> arg() const { return mArg; }
    SingleParamFunctionPtr function() const { return mFunction; }

private:
    Token mToken;
    SingleParamFunctionPtr mFunction;
    std::shared_ptr<Node> mArg;
};
}