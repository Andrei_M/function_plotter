TEMPLATE = subdirs

SUBDIRS = \
    function_parser_lib \
	test_function_parser_lib \
    function_plotter_app
CONFIG += ordered

function_plotter_app.depends = function_parser_lib
test_function_parser_lib.depends = function_parser_lib
