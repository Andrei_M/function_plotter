#include "catch.hpp"

#include "Lexer.h"
#include "Parser.h"
#include "Interpreter.h"
#include <QString>

fun::Interpreter makeInterpreter(std::string const & expression)
{
    auto lexer = fun::Lexer(expression);
    auto parser = fun::Parser(lexer);
    auto ast = parser.parse();
    return fun::Interpreter(ast);
}

TEST_CASE("Test expression", "interpreter")
{
    std::string expression = "15+3-4";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 14);
}

TEST_CASE("Test expression 2", "interpreter")
{
    std::string expression = "15+3 - 2 * (6-1)";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 8);
}

TEST_CASE("Test unary operators", "interpreter")
{
    std::string expression = "3--2";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 5);

    expression = "3 - + 2";
    result = makeInterpreter(expression).interpret();
    REQUIRE(result == 1);

    expression = "-3 -(2-5)";
    result = makeInterpreter(expression).interpret();
    REQUIRE(result == 0);
}

TEST_CASE("Test float", "interpreter")
{
    std::string expression = "3 / -2";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == -1.5f);
}

TEST_CASE("Test float 2", "interpreter")
{
    std::string expression = "13 + .2 - 1.5";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 11.7f);
}

TEST_CASE("Test power", "interpreter")
{
    std::string expression = "2^3";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 8);
}


TEST_CASE("Test power 2", "interpreter")
{
    std::string expression = "3^2*5^2";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 225);
}

TEST_CASE("Test power 3", "interpreter")
{
    std::string expression = "(3+2)^2*-2^3";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == -200);
}

TEST_CASE("Test power 4", "interpreter")
{
    std::string expression = "3^2^2";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 81);
}

TEST_CASE("Test power 5", "interpreter")
{
    std::string expression = "4*8^2*3";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 768);
}

TEST_CASE("Test power 6", "interpreter")
{
    std::string expression = "-3^2";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 9);
}

TEST_CASE("Test 3(5) as 3*5", "interpreter")
{
    std::string expression = "9-2(1+4)";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == -1);
}

TEST_CASE("Test 2((5)-(3)) as 2*5-2*3", "interpreter")
{
    std::string expression = "2((5)-(3))";
    auto result = makeInterpreter(expression).interpret();
    REQUIRE(result == 4);
}

TEST_CASE("Test var", "interpreter")
{
    std::string expression = "3*x+2";
    auto interp = makeInterpreter(expression);
    interp.setVarValue("x", 4);
    auto result = interp.interpret();
    REQUIRE(result == 14);
}

TEST_CASE("Test var 2", "interpreter")
{
    std::string expression = "3x";
    auto interp = makeInterpreter(expression);
    interp.setVarValue("x", 2);
    auto result = interp.interpret();
    REQUIRE(result == 6);
}

TEST_CASE("Test var 3", "interpreter")
{
    std::string expression = "3x^2";
    auto interp = makeInterpreter(expression);
    interp.setVarValue("x", 2);
    auto result = interp.interpret();
    REQUIRE(result == 12);
}

TEST_CASE("Test var 4", "interpreter")
{
    std::string expression = "x^2+3x-5";
    auto interp = makeInterpreter(expression);
    interp.setVarValue("x", 2);
    auto result = interp.interpret();
    REQUIRE(result == 5);
}

SCENARIO("Parsed AST is reused", "interpreter")
{
    GIVEN("an interpreter") {
        std::string expression = "3x^2 + 2x - 4";
        auto interp = makeInterpreter(expression);
        WHEN("x is 0") {
            interp.setVarValue("x", 0);
            THEN("the result is -4") {
                auto result = interp.interpret();
                REQUIRE(result == -4);
            }
        }

        WHEN("x is 1") {
            interp.setVarValue("x", 1);
            THEN("the result is 1") {
                auto result = interp.interpret();
                REQUIRE(result == 1);
            }
        }
    }
}