TEMPLATE = app
TARGET = test_function_parser_lib
CONFIG -= app_bundle
CONFIG += console

CONFIG(debug, debug|release) {
    LIBS += -L../function_parser_lib/debug -lfunction_parser
}
CONFIG(release, debug|release) {
    LIBS += -L../function_parser_lib/release -lfunction_parser
}

INCLUDEPATH += $$PWD/../function_parser_lib
INCLUDEPATH += $$PWD/../deps/catch

SOURCES = \
    main.cpp \
    test_lexer.cpp \
    test_interpreter.cpp
